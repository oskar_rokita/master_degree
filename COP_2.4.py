import RPi.GPIO as GPIO
from hx import HX711
from pyqtgraph.Qt import QtGui, QtCore
import numpy as np
import pyqtgraph as pg
import time
import threading
from tkinter.ttk import *
from tkinter import *
from tkinter import filedialog
import os
from datetime import datetime
import re
from math import sqrt
import random
import math

root = Tk()
root.title("Panel COP")
# Inicjalizacja zmiennych globalnych i nadanie poczatkowej wartosci
global x, y, are, bre, cre, dre, xmin, xmax, ymin, ymax, curr_path, r_range, off_poz, off_pion, pion, poziom, offx, offy , oczy
are = bre = cre = dre = n = xmin = xmax = ymin = ymax = os_cwiczen = strona_cop =score =off_poz=off_pion = pion = poziom = offx = offy = oczy =0
r_range = 30  # początkowy promien pkt zadanego
x = [0,0]         # współrzędne początkowe pukntów
y = [0,0]

global sum_tenso, sum_tenso_prev
sum_tenso_prev = 0
global xp, yp, bWasGood, cWasGood, poziom_p, pion_p, signal# Inicjalizacja zmiennych do filtra, xp i yp jako pretendenci do bycia x i y do wyswietlenia
bWasGood = cWasGood = signal = True # czy ostatni odczyt mieścił sie w zasiegu filtra, domyslnie 1 odczyt sie miesci
xp = yp = poziom_p = pion_p = 0
# Inicjalizacja klas HX dla kazdego prztwornika
# warningsy wylaczone bo tylko irytuja

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
hx = HX711(dout_pin=12, pd_sck_pin=26)
hxc= HX711(dout_pin=6, pd_sck_pin=5)
hxb= HX711(dout_pin=13, pd_sck_pin=19)
hxd= HX711(dout_pin=16, pd_sck_pin=20)
hx_pion = HX711(dout_pin=23, pd_sck_pin=18)
hx_poz = HX711(dout_pin=22, pd_sck_pin=17)
def tarowanie():
    # patrz hx.py
    global are, bre, cre, dre, off_poz, off_pion
    info_var.set('Tarowanie platformy, prosze zejsc!')
    root.update_idletasks()
    err = hx.zero()
    errb = hxb.zero()
    errc = hxc.zero()
    errd = hxd.zero()
    err_pion = hx_pion.zero()
    err_poz = hx_poz.zero()
    if err and errb and errc and errd and err_pion and err_poz:
        raise ValueError('Tarowanie nie powiodlo sie')
    are = hx.get_raw_data_mean()
    bre = hxb.get_raw_data_mean()
    cre = hxc.get_raw_data_mean()
    dre = hxd.get_raw_data_mean()
    off_poz = hx_poz.get_raw_data_mean()
    off_pion = hx_pion.get_raw_data_mean()
    if are and bre and cre and dre and off_pion and off_poz :
        print('Odczyty po ustawieniu przesuniecia :',
              are, ' ', bre, ' ', cre, ' ', dre,' ', off_poz,' ', off_pion)
    else:
        print('Blad odczytu po skalowaniu', are, ' ', bre, ' ', cre, ' ', dre)
    info_var.set("Tarowanie zakonczone\nProsze wejsc na platforme")
    root.update_idletasks()

def offsetowanie():
    global offx, offy
    info_var.set('Zerowanie platformy!')
    root.update_idletasks()
    offx = x[0]
    offy = y[0]
    info_var.set("Zerowanie zakonczone")
    root.update_idletasks()

def odczyt():
    global are,bre,cre,dre, off_pion,off_poz,pion_p, poziom_p
    global sum_tenso
    while True:
        a = hx._read()
        if a != False:      # czasami HX zwraca False przy odczycie i trzeba to odsiac bo inaczej traktuje jak int(0)
            aval= a-are
        b = hxb._read()
        if b != False:
            bval= b-bre
        c = hxc._read()
        if c != False:
            cval= c-cre
        d = hxd._read()
        if d != False:
            dval= d-dre

        e = hx_pion._read()
        if e != False:
            pion_p = e - off_pion
        f = hx_poz._read()
        if f != False:
            poziom_p = f - off_poz
        # wyliczanie COP na podstawie wszystkich 4 odczytow

        sum_tenso = aval+bval+cval+dval
        low_pass(sum_tenso,a,b,c,d,aval,bval,cval,dval)
        low_pass_pal()

def low_pass(sum_tenso,a,b,c,d,aval,bval,cval,dval):
    # Monitoruje skoki COP do 2 probek wstecz
    global bWasGood, os_cwiczen, cWasGood, sum_tenso_prev, x,y,xp,yp,offx,offy
    low_pass_range = 100000 # mozna dostroic w miare potrzeb
    if ((sum_tenso-sum_tenso_prev)) not in range (-low_pass_range,low_pass_range) and (bWasGood or cWasGood):
        if not bWasGood:
            cWasGood = False
        bWasGood = False
    else:
        sum_tenso_prev = sum_tenso
        if sum_tenso <= 0:
            sum_tenso = 1
        x1 = int(bval) + int(aval)
        x2 = int(dval) + int(cval)
        y1 = int(aval) + int(cval)
        y2 = int(bval) + int(dval)
        if a != False and b != False and c!=False and d != False:
            xp = ((y2 - y1)/(sum_tenso)/2*433)-offx
            yp = ((x1 - x2)/(sum_tenso)/2*228)-offy
        bWasGood = True
        cWasGood = True
        if sum_tenso>50000:
            if os_cwiczen ==1:
                x[0] = 0
                y[0] = yp
            elif os_cwiczen ==2:
                x[0] = xp
                y[0] = 0
            else :
                x[0] = xp
                y[0] = yp
        else:
            x[0] = 0
            y[0] = 0

def low_pass_pal():
    global poziom_p,pion_p,pion,poziom,signal
    low_tenso_range = 160000
    if (pion_p) in range(-low_tenso_range,low_tenso_range) and (poziom_p) in range (-low_tenso_range,low_tenso_range):
        pion = pion_p
        poziom = poziom_p
        signal =True

def generate_theta(a,b):
    # patrz post Stackoverflow https://stackoverflow.com/questions/5529148/algorithm-calculate-pseudo-random-point-inside-an-ellipse
    u = random.random() / 4.0
    theta = np.arctan(b/a * np.tan(2 * np.pi*u))

    v = random.random()
    if v < 0.25:
        return theta
    elif v < 0.5:
        return np.pi - theta
    elif v < 0.75:
        return np.pi + theta
    else:
        return -theta

def radius(a,b,theta):
    # patrz post Stackoverflow https://stackoverflow.com/questions/5529148/algorithm-calculate-pseudo-random-point-inside-an-ellipse
    return int(a * b / np.sqrt((b*np.sqrt((b*np.cos(theta))**2)) + (a*np.sin(theta))**2))

def random_point(a,b):
    # patrz post Stackoverflow https://stackoverflow.com/questions/5529148/algorithm-calculate-pseudo-random-point-inside-an-ellipse
    random_theta = generate_theta(a,b)
    max_radius = radius(a,b,random_theta)
    random_radius = max_radius * np.sqrt(random.random())

    return np.array([
        int(random_radius * np.cos(random_theta)),
        int(random_radius * np.sin(random_theta))
        ])

def pkt_zadany():
    # patrz post Stackoverflow https://stackoverflow.com/questions/5529148/algorithm-calculate-pseudo-random-point-inside-an-ellipse
    global x, y, xmax,xmin,ymax,ymin,last_left
    if ex_axis.get()==1:
        x[1] = 0
        y[1] = np.random.randint(ymin,0)
        last_left = True
    elif ex_axis.get()==2:
        x[1] = np.random.randint(xmin,0)
        y[1] = 0
        last_left = True
    else:
        a = (abs(xmin)+xmax)/2      # Wyliczanie średniej z max i min moze generować błąd:  jeśli są duże róznice między zasięgami min/max w jednej płaszczyżnie
        b = (abs(ymin)+ymax)/2      # jest opcja wylosowania punktu poza zasięgiem. Tym dalej zasięgu będzie im większa jest dysproporcja między min/max .
        random_theta = generate_theta(a,b)
        max_radius = radius(a,b,random_theta)
        random_radius = max_radius * np.sqrt(random.random())
        x[1] = int(random_radius * np.cos(random_theta))
        y[1] = int(random_radius * np.sin(random_theta))
        last_left = True

def cwiczenie_1():
    global x, y, n, last_left,curr_path,r_range,score, pion, poziom
    last_left = False
    inter_points = []
    inter_time = []
    model_vel = 100  # predkosc przenoszenie sr. nacisku zdrowego czlowieka. Wzorzec do porownania. [mm/s] podane na oko
    update_mode()
    score = 0
    n=0
    c_start = time.time()
    info_var.set('Trwa cwiczenie na poziomie: '+str(diff_lvl.get()))
    zakres = int((6-diff_lvl.get())*5) # Promien poziom : 1. 25mm  2. 20mm  3. 15mm  4. 10mm  5. 5mm
    r_range=zakres*2
    pkt_zadany()
    end_time = c_cwiczenia.get()*10
    czas_zaliczenia = c_zaliczenia.get()
    while c_start + end_time > time.time():
        if ex_axis.get() == 1:
            if last_left:
                inter_points.append(str(round(y[1]/abs(ymin)*100,2)))
            else:
                inter_points.append(str(round(y[1]/ymax*100,2)))
        elif ex_axis.get() == 2:
            if last_left:
                inter_points.append(str(round(x[1]/abs(xmin)*100,2)))
            else:
                inter_points.append(str(round(x[1]/xmax*100,2)))
        else:
            inter_points.append(str(round(x[1]/xmax*100,2))+" "+str(round(y[1]/ymax*100,2)))
        prev_x = x[1]
        prev_y = y[1]
        czas = int(time.time()-c_start)
        czas_var.set("Czas: "+str(czas)+"[s]")
        points_var.set("Punkty: "+str(n))
        bar_1["value"]=(pion/1000)+150
        bar_2["value"]=(poziom/1000)+150
        root.update_idletasks()
        czas_utrzymania = time.time()
        time_get = time.time()
        while czas_utrzymania + c_zaliczenia.get() > time.time():
            if int(x[1]-x[0]) not in range(-zakres,zakres) or int(y[1]-y[0]) not in range(-zakres,zakres):
                czas_utrzymania = time.time()
                czas = int(time.time()-c_start)
                czas_var.set("Czas: "+str(czas)+"[s]")
                progress['value'] = 0
                bar_1["value"]=(pion/1000)+150
                bar_2["value"]=(poziom/1000)+150
                root.update_idletasks()
            czas = int(time.time()-c_start)
            czas_var.set("Czas: "+str(czas)+"[s]")
            progress['value'] = (time.time()-czas_utrzymania)*100/czas_zaliczenia
            bar_1["value"]=(pion/1000)+150
            bar_2["value"]=(poziom/1000)+150
            root.update_idletasks()
        time_patient = round(time.time()-time_get-c_zaliczenia.get(),4)
        inter_time.append(time_patient)
        n += 1
        score += score + (sqrt(int(x[1]**2+y[1]**2))*time_patient)/2 -(int(x[1]**2+y[1]**2)/model_vel) # pole pacjenta - pole wzorcowe
        points_var.set("Punkty: "+str(n))
        root.update_idletasks()
        x[1] = 0
        y[1] = 0
        czas_utrzymania = time.time()
        while czas_utrzymania + 1 > time.time() and  c_start + end_time > time.time():
            if int(x[1]-x[0]) not in range(-zakres,zakres) or int(y[1]-y[0]) not in range(-zakres,zakres):
                czas_utrzymania = time.time()
                czas = int(time.time()-c_start)
                czas_var.set("Czas: "+str(czas)+"[s]")
                bar_1["value"]=(pion/1000)+150
                bar_2["value"]=(poziom/1000)+150
                root.update_idletasks()
        if last_left :
            x[1] = -prev_x
            y[1] = -prev_y
            last_left = False
        else:
            pkt_zadany()
    info_var.set('Cwiczenie zakonczone! Wynik: '+ str(int(score/1000)) +" [CWS]")
    root.update_idletasks()
    x[1] = 0
    y[1] = 0
    time.sleep(2)
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y")
    inx,strona = opt[os_cwiczen-1]
    with open(curr_path+'_inter1.txt','a') as file:
        file.write(dt_string+" plaszczyzna: "+strona+'\n')
        for idx in range (len(inter_time)):
            file.write(str(inter_points[idx])+' '+str(inter_time[idx])+'\n')
    file.close()
    info_var.set('Zapisano wynik')
    root.update_idletasks()
    zapisz_wynik_cw1()
    root.update_idletasks()

def cwiczenie_2():
    global x, y, n, last_left,curr_path,r_range,pion, poziom
    last_left = False
    inter_overshoot = []
    inter_vel = []
    inter_points = []
    update_mode()
    n=0
    c_start = time.time()
    info_var.set('Trwa cwiczenie na poziomie: '+str(diff_lvl.get()))
    zakres = int((6-diff_lvl.get())*5)
    r_range = zakres*2
    prev_x = 0
    prev_y = 0
    pkt_zadany()
    end_time = c_cwiczenia.get()*10
    while c_start + end_time > time.time():
        bit_ = True
        overshoot = 0
        czas = int(time.time()-c_start)
        czas_var.set("Czas: "+str(czas)+"[s]")
        points_var.set("Punkty: "+str(n))
        bar_1["value"]=(pion/1000)+150
        bar_2["value"]=(poziom/1000)+150
        root.update_idletasks()
        czas_utrzymania = time.time()
        time_get = time.time()        # Start zegara mierzącego czas osiągania punktu
        while czas_utrzymania + 0.2 > time.time():
            if ex_axis.get() == 2:
                if int(x[0]*x[1])>0 and abs(x[0])>abs(x[1]) and (abs(x[0])-abs(x[1]))>overshoot:
                    overshoot = abs(x[0])-abs(x[1])
            if ex_axis.get() == 1:
                if int(y[0]*y[1])>0 and abs(y[0])>abs(y[1]) and (abs(y[0])-abs(y[1]))>overshoot:
                    overshoot = abs(y[0])-abs(y[1])
            if ex_axis.get() == 3:
                if int(x[0]*x[1])>0 and y[0]*y[1]>0 and sqrt(x[0]**2+y[0]**2)-sqrt(x[1]**2+y[1]**2)>overshoot :
                    overshoot = sqrt(x[0]**2+y[0]**2)-sqrt(x[1]**2+y[1]**2)
            if int(x[1]-x[0]) not in range(-zakres,zakres) or int(y[1]-y[0]) not in range(-zakres,zakres):
                czas_utrzymania = time.time()
                czas = int(time.time()-c_start)
                czas_var.set("Czas: "+str(czas)+"[s]")
                progress['value'] = 0
                bar_1["value"]=(pion/1000)+150
                bar_2["value"]=(poziom/1000)+150
                root.update_idletasks()
            elif (int(x[1]-x[0]) in range(-zakres,zakres) or int(y[1]-y[0]) in range(-zakres,zakres)) and bit_:
                vel = sqrt((abs(prev_x)+abs(x[1]))**2 + (abs(prev_y) + abs(y[1]))**2)/(time.time() - time_get)
                bit_ = False
            progress['value'] = (time.time()-czas_utrzymania)*200
            bar_1["value"]=(pion/1000)+150
            bar_2["value"]=(poziom/1000)+150
            root.update_idletasks()
        if ex_axis.get() == 1:
            inter_points.append(str(y[1]))
        elif ex_axis.get() == 2:
            inter_points.append(str(x[1]))
        else:
            inter_points.append(str(x[1])+" "+str(y[1]))
        if vel < 300:                  # eliminuje wielkie predkosci jesli kolejny pkt wylosuje się tak blisko poprzedniego że od razu sie zalicza ( powoduje to obliczenie predkosci na 30000 mm/s)
            inter_vel.append(int(vel))
        if overshoot < 100:           # to samo z overshootem, jesli akurat zaszumi układ to nie zapisze uchybu wiekszego niz 10 cm
            inter_overshoot.append(int(overshoot))
        prev_x = x[1]
        prev_y = y[1]
        n += 1
        points_var.set("Punkty: "+str(n))
        root.update_idletasks()

        if last_left :
            x[1] = -prev_x
            y[1] = -prev_y
            last_left = False
        else:
            pkt_zadany()
    info_var.set('Cwiczenie zakonczone! Wynik: '+ str(n) +"pkt/min")
    root.update_idletasks()
    x[1] = 0
    y[1] = 0
    time.sleep(2)
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y")
    inx,strona = opt[os_cwiczen-1]
    mean_vel_left = mean_vel_right = mean_overshoot_left = mean_overshoot_right = 0
    for i in range (len(inter_vel)):
        if i%2 != 0:
            mean_vel_right += inter_vel[i]
        else:
            mean_vel_left += inter_vel[i]
    for i in range (len(inter_overshoot)):
        if i%2 != 0:
            mean_overshoot_right += inter_overshoot[i]
        else:
            mean_overshoot_left += inter_overshoot[i]
    with open(curr_path+'_inter2.txt','a') as file:
        file.write(dt_string+" plaszczyzna: "+strona+'\n')
        #file.write(str(inter_points[i])+' '+str(inter_vel[i])+' '+str(inter_overshoot[i])+'\n')
        file.write("sr. pred. prawo: "+str(round(mean_vel_right/len(inter_vel),2))+"[mm/s] sr. pred. lewo: "+str(round(mean_vel_left/len(inter_vel),2))+
                    "[mm/s] sr. uchyb prawo: "+str(round(mean_overshoot_right/len(inter_overshoot),2))+"[mm] sr. uchyb lewo: "+str(round(mean_overshoot_left/len(inter_overshoot),2))+"[mm]")
    file.close()
    info_var.set('Zapisano wynik')
    root.update_idletasks()
    zapisz_wynik_cw2()

def zapisz_wynik_cw1():
    global curr_path, n, xmax, xmin, ymax, ymin, score
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y")
    idx,strona = opt[os_cwiczen-1]
    with open(curr_path+'.txt','a') as file:
        file.write(dt_string+' zakres: X('+str(xmin)+','+str(xmax)+') Y('+str(ymin)+','+str(ymax)+")"+' poziom '+ str(diff_lvl.get())+' wynik '+ str(n) +' plaszczyzna: '+strona+' czas_cw[sek] '+ str(c_cwiczenia.get()*10)+" CWS "+str(int(score/1000))+"\n")
    info_var.set('Zapisano wynik')
    root.update_idletasks()
    file.close()

def zapisz_wynik_cw2():
    global curr_path, n, xmax, xmin
    idx,strona = opt[os_cwiczen-1]
    now = datetime.now()
    dt_string = now.strftime("%d/%m/%Y")
    with open(curr_path+'.txt','a') as file:
        file.write(dt_string+' zakres: X('+str(xmin)+','+str(xmax)+') Y('+str(ymin)+','+str(ymax)+') '+'poziom '+str(diff_lvl.get())+' wynik '+ str(n)+' plaszczyzna '+strona+' czas_cw[sek] '+ str(c_cwiczenia.get()*10)+"\n")
    info_var.set('Zapisano wynik')
    root.update_idletasks()
    file.close()

def plot():
    global spot, p1,r_range
    win = pg.GraphicsWindow(title="COP") # tworzy okno graficzne
    win.resize(1100,940)                 # rozmiar okna w pix
    p1 = win.addPlot(title="Srodek nacisku") # dodaje okno wykresu
    p1.setRange(xRange=(-250,250),yRange=(-250,250),disableAutoRange=True) # ustawia zajkres X i Y , wyłącza autoskalowanie
    p1.showGrid(x=True, y=True) # pokazuj siatke
    spot = pg.ScatterPlotItem(x,y,pxMode=False,size=[10,12],brush='r')   # tworzy wykres kropkowy , kropki o wymiarach 10 i 30,
    p1.addItem(spot) # Wstawia wykres w okno wykresu
    def update(): # do aktualizacji wykresu
        global spot, x, y, p1,r_range
        # pobieramy pozycje x i y ze zmiennych globalnyhc , promień pkt zadanego tez, drugi promień jest stały
        pkt = [{'pos':(x[1],y[1]),'size':r_range,'brush':'r'},{'pos':(x[0],y[0]),'size':10,'brush':'b'}]
        spot.setData(pkt) # ustawiamy nowe pozycje punków
    # tworzy zegar odliczajacy 10ms, co taki czas uruchamia update()
    timer = QtCore.QTimer()
    timer.timeout.connect(update)
    timer.start(10)
    QtGui.QApplication.instance().exec_()

def user_config():
    global xmax, xmin, ymax, ymin, x, y
    xmax = xmin = ymax = ymin = 0
    info_var.set('Wychyl sie maksymalnie do przodu.')
    root.update_idletasks()
    conf_time = time.time()
    while conf_time + 5 > time.time():
        if y[0]>ymax:
            ymax = int(y[0])
            print(ymax)
    info_var.set('Wroc na srodek.')
    root.update_idletasks()
    time.sleep(2)
    info_var.set('Wychyl sie maksymalnie do tylu.')
    root.update_idletasks()
    conf_time = time.time()
    while conf_time + 5 > time.time():
        if y[0]<ymin:
            ymin = int(y[0])
            print(ymin)
    info_var.set('Wroc na srodek.')
    root.update_idletasks()
    time.sleep(2)
    info_var.set('Wychyl sie maksymalnie w lewo.')
    root.update_idletasks()
    conf_time = time.time()
    while conf_time + 5 > time.time():
        if x[0]<xmin:
            xmin = int(x[0])
            print(xmin)
    info_var.set('Wroc na srodek.')
    root.update_idletasks()
    time.sleep(2)
    info_var.set('Wychyl sie maksymalnie w prawo.')
    root.update_idletasks()
    conf_time = time.time()
    while conf_time + 5 > time.time():
        if x[0]>xmax:
            xmax = int(x[0])
            print(xmax)
    info_var.set('Ustawiono zakres: \nX('+str(xmin)+','+str(xmax)+') \nY('+str(ymin)+','+str(ymax)+')')
    root.update_idletasks()

def nowy_user():
    def save_user(imie,nazwisko):
        global curr_path
        path = 'pacjenci'
        if not os.path.exists(path): # jesli nie ma jeszcze folderu pacjentow to do tworzy
            os.makedirs(path)
        if imie[:6]=='Imie: ': # zabezpieczenie przez skasowaniem liter w polu, inaczej ucina imie pacjenta
            f = open(os.path.join(path,imie[6:]+'_'+nazwisko[9:]+'.txt'),'w')
            curr_path = os.path.join(path,imie[6:]+'_'+nazwisko[9:])
        else:
            f = open(os.path.join(path,imie+'_'+nazwisko+'.txt'),'w')
            curr_path = os.path.join(path,imie+'_'+nazwisko)
        info_var.set("Utworzono plik pacjenta")
        root.update_idletasks()
        f.close()
    top = Toplevel()
    top.title('Nowy pacjent')
    imie = Entry(top, width=50,bg='green',fg='white',borderwidth=5)
    nazwisko = Entry(top, width=50,bg='green',fg='white',borderwidth=5)
    imie.insert(0, "Imie: ")
    nazwisko.insert(0, 'Nazwisko: ')
    imie.grid(row=0,column=0,columnspan=2,padx=60,pady=20)
    nazwisko.grid(row=1,column=0,columnspan=2,padx=60,pady=20)
    save = Button(top, text='Zapisz',command=lambda: save_user(imie.get(),nazwisko.get()))
    btn2 = Button(top, text=('Zamknij okno'),command=top.destroy)
    save.grid(row=2,column=0,padx=30,pady=20)
    btn2.grid(row=2,column=1,padx=30,pady=20)

def pick_user():
    global curr_path
    curr_path = filedialog.askopenfilename(initialdir="/pacjenci",title="Wybierz plik",filetypes=(("plik tekstowy","*.txt"),("all files","*.*")))
    curr_path = curr_path[:-4]
    info_var.set("Wybrano pacjenta")
    root.update_idletasks()

def last_config():
    global curr_path, xmin, xmax, ymin, ymax
    with open (curr_path+'.txt','r') as file:
        first_line = file.readline()
        for last_line in file:
            pass
        # wzorce do szukania odpowiednich liczb w pliku, mozna w zapsie dodać
        # jakies znaki szczegolne zeby to łatwiej znajdować np( $ % ^)
        patxmin = "X\((.*?)\,"
        patxmax = "\,(.*?)\) Y"
        patymin = "Y\((.*?)\,"
        patymax = "\,(.*?)\)"
        xmin = int(re.search(patxmin,last_line).group(1))
        xmax = int(re.search(patxmax,last_line).group(1))
        ymin = int(re.search(patymin,last_line).group(1))
        ymax = int(re.search(patymax,last_line.split()[3]).group(1))
    info_var.set("Wczytano poprzednia konfiguracje"+'\n zakres: X('+str(xmin)+','+str(xmax)+')\n Y('+str(ymin)+','+str(ymax)+')')

def update_mode():
    # po odpaleniu cwiczenia aktualizauje zmienna od plaszczyzny cwiczen
    global os_cwiczen, strona_cop, oczy
    os_cwiczen = ex_axis.get()
    oczy = oczy_var.get()

def delete_last():
    fd = open(curr_path+'.txt','r')
    d = fd.read()
    fd.close()
    m = d.split("\n")
    s = "\n".join(m[:-2])
    fd = open(curr_path,'w+')
    for i in range(len(s)):
        fd.write(s[i])
    fd.write('\n')
    fd.close()
    info_var.set('Usunieto ostatni pomiar')
    root.update_idletasks()

def cwiczenie_3():
    update_mode()

    def cw3_interpreter(P):
        global curr_path, oczy
        dfi=360
        roz= 1 #wielkość kąta dfi (rozdzielczość)
        rapr= 0.1 #rozdzielczość aproksymacji
        r=[] #maciesz długości wektorów
        f=[] # macierz kątów
        wr=[]
        wf=[]
        ax=[]
        ay=[]
        x=[]
        y=[]
        s=[]
        r1=[]
        n=len(P)
        maxxp=maxyp=maxxpm=maxypm=srxx=sryy=i=droga=0
        while i<n-1:
                e1=math.sqrt(((P[i][0])**2)+(P[i][1]**2))
                r1.append(e1)
                sx=abs(P[i][0])-abs(P[i+1][0])
                sy=abs(P[i][1])-abs(P[i+1][1])
                ss=math.sqrt(sx**2+sy**2)
                droga=ss+droga
                s.append(ss)
                if ss>rapr:
                    ds=ss/rapr
                    rds=round(ds,0)
                    rx=(P[i+1][0]-P[i][0])/rds
                    x.append(P[i][0])
                    y.append(P[i][1])
                    if (P[i+1][0]-P[i][0])!=0 and (P[i+1][1]-P[i][1])!=0:
                        j=0
                        slope=(P[i+1][1]-P[i][1])/(P[i+1][0]-P[i][0])
                        b=-slope*P[i][0]+P[i][1]
                        while j<rds:
                            nx=P[i][0]+rx+rx*j
                            ny=slope*nx+b
                            x.append(nx)
                            y.append(ny)
                            j=j+1
                    elif P[i+1][0]-P[i][0]==0:
                        j=0
                        while j<rds:
                            nx=P[i][0]
                            ny=P[i][1]+((rx+rx*j)*(P[i][1]-P[i+1][1])/abs((P[i][1]-P[i+1][1])))
                            x.append(nx)
                            y.append(ny)
                            j=j+1
                    elif P[i+1][1]-P[i][1]==0:
                        j=0
                        while j<rds:
                            ny=P[i][1]
                            nx=P[i][0]+((rx+rx*j)*(P[i][0]-P[i+1][0])/abs((P[i][0]-P[i+1][0])))
                            x.append(nx)
                            y.append(ny)
                            j=j+1
                else:
                    x.append(P[i][0])
                    y.append(P[i][1])
                srx=P[i][0]
                sry=P[i][1]
                srxx=srx+srxx #suma do średniej wartości
                sryy=sry+sryy #suma do średniej wartości
                if srx>maxxp:
                    maxxp=srx
                if sry>maxyp:
                    maxyp=sry
                if srx<maxxpm:
                    maxxpm=srx
                if sry<maxypm:
                    maxypm=sry
                i=i+1
        tpocz=P[0][2]
        tkon=P[n-1][2]
        dt=tkon-tpocz
        if droga!=0:
            v=droga/dt
        else:
            v=0
        i=0
        n=len(x)
        while i<n:
            e=math.sqrt(((x[i])**2)+(y[i]**2))
            r.append(e)
            if r[i]!=0:
                fi=math.acos((x[i])/r[i])*180/math.pi
            else:
                fi=0
            if y[i]<0:
                fi=360-fi
            f.append(fi)
            i=i+1
        i=roz
        while i<dfi+roz: #wyznaczanie punktu najbardziej oddalonego od środka
            pre=i-roz
            j=wm=wx=wy=ppmax=wm1=0
            while j<n:
                if f[j]>pre and f[j]<=i:
                    pmax=r[j]
                    if pmax>ppmax:
                        ppmax=pmax
                        wm=f[j]
                        wx=x[j]
                        wy=y[j]
                j=j+1
            if ppmax!=0:
                ax.append(wx)
                ay.append(wy)
                wr.append(ppmax)
                wf.append(wm)
            i=i+roz
        n=len(wr)
        i=1
        pole=0
        while i<n: #obliczanie pola

            pole=pole+wr[i-1]*wr[i]*math.sin((wf[i]-wf[i-1])*math.pi/180)/2
            if i==n-1:
                pole=pole+wr[0]*wr[n-1]*math.sin(abs((wf[0]-wf[n-1]))*math.pi/180)/2
            i=i+1
        i=1
        n=len(r1)
        ttc=0
        while i<(n-1): #obliczanie czasu
            if r1[i-1]>25:
                ttc+=P[i][2]-P[i-1][2]
            i=i+1
        print('Pole powierzchni: ',round(pole,2),'mm2')
        print('Przebyta droga: ',round(droga,2),'mm')
        print('Średnia prędkość: ',round(v,2),'mm/s')
        print('Min x: ',round(maxxpm,2),'mm')
        print('Max x: ',round(maxxp,2),'mm')
        print('Min y: ',round(maxypm,2),'mm')
        print('Max y: ',round(maxyp,2),'mm')
        print('Średnie wychylenie w płaszyźnie czołowej: ',round(sryy/n,2),'mm')
        print('Średnie wychylenie w płaszczyźnie strzałkowej: ',round(srxx/n,2),'mm')
        print('Czas przebywania poza okręgiem 25 mm : ',round(ttc,2),'s')
        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y")
        idx,variant = options[oczy-1]
        with open(curr_path+'_cw3.txt','a') as file:
            file.write('\n'+dt_string+' cw 3. '+ variant+"\nPole powierzchni: "+ str(round(pole,2))+"[mm^2] Droga: "+str(round(droga,2))+'[mm]'+
                        'śr. pred: '+str(round(v,2))+'[mm/s] \nMin x :'+str(round(maxxpm,2))+" Max x: "+str(round(maxxp,2))+" [mm]\n"+
                        'Min y: '+str(round(maxypm,2))+' Max y: '+str(round(maxyp,2))+'\nśr. wych pl. czołowa: '+str(round(sryy/n,2))+
                        "\nśr. wych pl. strzałkowa: "+str(round(srxx/n,2))+"\nCzas poza okregiem: "+str(round(ttc,2)))


    global x, y, pion, poziom, signal,r_range
    global aval,bval,cval,dval
    ratio = 7115/160750 #0.00042447 jednostki/g
    r_range = 25
    x_max = x_min = y_min = y_max = 0
    vertical_tenso = 0
    horizontal_tenso = 0
    c_start = time.time()
    info_var.set("Trwa cwiczenie 3")
    root.update_idletasks()
    end_time = c_cwiczenia.get()*10
    n=0
    punkty = [[0,0,0]]
    start_time=0
    poczatek = time.time()
    while c_start + end_time > time.time():
        if signal:
            start_time = time.time()
            if x[0]<30 and y[0]<30 :
                punkty.append([round(x[0],2),round(y[0],2),round(start_time-poczatek,4)])
                if x[0] > x_max :
                    x_max = (x[0])
                if x[0] < x_min :
                    x_min = (x[0])
                if y[0] < y_min :
                    y_min = (y[0])
                if y[0] > y_max :
                    y_max = (y[0])
                if poziom > 1000:
                    horizontal_tenso += int(poziom*ratio)
                if pion > 1000:
                    vertical_tenso += int(pion*ratio)
            bar_1["value"]=(pion/1000)+150
            bar_2["value"]=(poziom/1000)+150
            root.update_idletasks()
            n+=1
            signal = False
        else:
            pass
    #tu jest po cwiczeniu
    x_max = round(x_max,2)
    x_min = round(x_min,2)
    y_max = round(y_max,2)
    y_min = round(y_min,2)
    horizontal_tenso = round(horizontal_tenso/n,2)
    vertical_tenso = round(vertical_tenso/n,2)
    #now = datetime.now()
    # with open("cwiczenie_3.txt",'a') as file:
    #     file.write(str(vertical_tenso)+', '+str(horizontal_tenso)+', '+str(x_max)+', '+str(x_min)+', '+str(y_max)+', '+str(y_min)+"\n")
    # file.close()
    # with open ("cw_3_pkt.txt","a") as plik:
    #     for item in range (0,len(punkty)):
    #         plik.write(str(punkty[item])+"\n")
    #plik.close()
    info_var.set("Dryf Y: "+str(y_max)+" "+str(y_min)+" X: "+str(x_max)+" "+str(x_min))
    root.update_idletasks()
    time.sleep(5)
    info_var.set("Srednie Oparcie pionowe: "+str(vertical_tenso)+"[gr/n] | Poziome: "+str(horizontal_tenso)+"[gr/n]")
    print("n = ",n)
    root.update_idletasks()
    time.sleep(3)
    cw3_interpreter(punkty)



# Zmienne tekstowe i liczbowe do aktualizacji paskow i progressbarów

czas_var = StringVar()
points_var = StringVar()
info_var = StringVar()
diff_lvl = IntVar()
c_cwiczenia = IntVar()
c_zaliczenia = IntVar()
czas_var.set('Czas')
points_var.set('Punkty')
info_var.set('Aktualna czynnosc')
# Tu nie ma nic trudnego
info = Label(root, textvariable=info_var,font=('arial', 15, "bold"))
pl_cw = Label(root, text='Wybierz plaszczyzne cwiczen: ',font=('arial', 10))
points = Label(root, textvariable=points_var,font=('arial', 10))
czas = Label(root, textvariable=czas_var,font=('arial', 10))
progress = Progressbar(root, orient = HORIZONTAL,length = 100, mode = 'determinate')
delete = Button(root,text='Usun ostatni pomiar',font=('arial', 8), command = delete_last)
tara = Button(root, text='Tarowanie platformy',font=('arial', 8), command = offsetowanie)
konfig = Button(root, text='Konfiguracja uzytkownika',font=('arial', 8), command = user_config)
level = Scale(root, from_=1,to=5,orient=HORIZONTAL,length=150,label='Poziom trudnosci',font=('arial', 8),variable=diff_lvl)
start_butt = Button(root, text='Start cwiczenia 1',font=('arial', 8), command = cwiczenie_1)
new_user = Button(root, text='Nowy pacjent',font=('arial', 8),command = nowy_user)
load_user = Button(root, text='Wybierz pacjenta',font=('arial', 8), command = pick_user)
start_cop = Button(root,text='Start cwiczenia 2',font=('arial', 8), command = cwiczenie_2)
start_cw3 = Button(root,text='Start cwiczenia 3',font=('arial', 8), command = cwiczenie_3)
last_config = Button(root,text='Wczytaj ostatnie zakresy',font=('arial', 8), command = last_config)
cw_1 = Label(root,text='Cwiczenie 1. Utrzymanie srodka nacisku w poblizu wylosowanego punktu przez 2 sek\nz powrotem na srodek: ',font=('arial', 11, "bold"))
cw_2 = Label(root,text='Cwiczenie 2. Jak najszybsze przenoszenie srodka nacisku w plaszczyznie cwiczen\nprzy jak najmniejszym przeregulowaniu : ',font=('arial', 11, "bold"))
cw_3 = Label(root,text='Cwiczenie 3. Utrzymanie środka nacisku wewnątrz okręgu : ',font=('arial', 11, "bold"))
level2 = Scale(root, from_=1,to=5,orient=HORIZONTAL,length=150,label="Czas cwiczenia [sek*10]",font=('arial', 8),variable=c_cwiczenia)
czas_utrzymania = Scale(root, from_=1,to=3,orient=HORIZONTAL,length=150,label="Czas zaliczenia [sek]",font=('arial', 8),variable=c_zaliczenia)
# Deklaracja pasków postępu do wizualizacji sił na tensometrach 200kg
# dlugosci i maxmima mozna dostrajac
bar_1 = Progressbar(root, orient = HORIZONTAL,length = 200, mode = 'determinate')
bar_1["maximum"] = 300
bar_2 = Progressbar(root, orient = HORIZONTAL,length = 200, mode = 'determinate')
bar_2["maximum"]=300

root.update_idletasks()

info.grid(row=0,column=0,columnspan=3,padx=60,pady=40)
points.grid(row=1,column=0,padx=10,pady=10)
czas.grid(row=1,column=1,padx=10,pady=10)
progress.grid(row=1,column=2,padx=10,pady=10)
tara.grid(row=2,column=0,padx=10,pady=10)
pl_cw.grid(row=3,column=0,columnspan=3,padx=60,pady=10)
cw_1.grid(row=6,column=0,columnspan=3,padx=60,pady=10)
konfig.grid(row=2,column=1,padx=10,pady=10)
delete.grid(row=2,column=2,padx=10,pady=10)
level.grid(row=5,column=0,padx=10,pady=10)
opt = [(1,'Strzalkowa'),(2,'Czolowa'),(3,'Obydwie')]
buttons1 = []
ex_axis = IntVar()
ex_axis.set(3)
for idx, (mode,text) in enumerate(opt):
    buttons1.append(Radiobutton(root,padx=10,pady=10,font=('arial', 10),text=text,variable=ex_axis,value=mode,indicatoron=1))
    buttons1[-1].grid(row=4,column=idx)
start_butt.grid(row=7,column=0,columnspan=3,padx=60,pady=10)
cw_2.grid(row=8,column=0,columnspan=3,padx=60,pady=10)
level2.grid(row=5,column=1,padx=50,pady=10)
czas_utrzymania.grid(row=5,column=2,padx=50,pady=10)
start_cop.grid(row=9,column=0,columnspan=3,padx=60,pady=20)
new_user.grid(row=10,column=0,padx=10,pady=10)
load_user.grid(row=10,column=1,padx=10,pady=10)
last_config.grid(row=10,column=2,padx=10,pady=10)
bar_1.grid(row=11,column=0,columnspan=3,padx=100,pady=20)
bar_2.grid(row=12,column=0,columnspan=3,padx=100,pady=20)
cw_3.grid(row=13,column=0,columnspan=3,padx=60,pady=10)
start_cw3.grid(row=15,column=0,columnspan=3,padx=60,pady=20)
options = [(1,'Oczy zamkniete'),(2,'Oczy otwarte')]
buttons_3 = []
oczy_var = IntVar()
oczy_var.set(1)
for idx, (mode,text) in enumerate(options):
    buttons_3.append(Radiobutton(root,padx=10,pady=10,font=('arial',10),text=text,variable=oczy_var,value=mode,indicatoron=1))
    buttons_3[-1].grid(row=14,column=idx)
tarowanie()

t1 = threading.Thread(target=odczyt)
t2 = threading.Thread(target=plot)
t1.start()
t2.start()

root.mainloop()
